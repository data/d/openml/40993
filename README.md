# OpenML dataset: ada_agnostic

https://www.openml.org/d/40993

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: [Isabelle Guyon](isabelle@clopinet.com)  
**Source**: [Agnostic Learning vs. Prior Knowledge Challenge](http://www.agnostic.inf.ethz.ch)  
**Please cite**: None

__Major change w.r.t. version 1: updated data type of binary variables to factor type.__

Dataset from the Agnostic Learning vs. Prior Knowledge Challenge (http://www.agnostic.inf.ethz.ch), which consisted of 5 different datasets (SYLVA, GINA, NOVA, HIVA, ADA). The purpose of the challenge was to check if the performance of domain-specific feature engineering (prior knowledge) can be met by algorithms that were trained on data without any domain-specific knowledge (agnostic). For the latter, the data was anonymised and preprocessed in a way that makes them uninterpretable. 

This dataset contains the agnostic (smashed) version of a data set from the US census bureau for the time span June 2005 - September 2006. Similar data set on OpenML is called __adult__. The raw data from the census bureau is also known as the Adult database in the UCI machine-learning repository. 

### Topic

The task of ADA is to discover high revenue people from census data. This is a two-class
classification problem. The raw data from the census bureau is known as the Adult
database in the UCI machine-learning repository. It contains continuous, binary and
categorical variables. The “prior knowledge track” has access to the original features and
their identity. The agnostic track has access to a preprocessed numeric representation
eliminating categorical variables. 

### Source

Original owners
This data was extracted from the census bureau database found at
http://www.census.gov/ftp/pub/DES/www/welcome.html
Donor: Ronny Kohavi and Barry Becker,
 Data Mining and Visualization
 Silicon Graphics.
 e-mail: ronnyk@sgi.com for questions

Dataset from: http://www.agnostic.inf.ethz.ch/datasets.php

### Preprocessing

In [this documentation](http://clopinet.com/isabelle/Projects/agnostic/Dataset.pdf) the organisers of the challenge describe the steps they performed to come up with the __agnostic__ data. The 14 original attributes (features) include age, workclass,  education,
marital status, occupation, native country, etc. It contains continuous, binary and categorical features. This dataset is from the "agnostic learning track", i.e. has access to a preprocessed numeric representation eliminating categorical variables, but the identity of the features is not revealed. Furthermore, features are scrambled and cannot be linked to the features given in the dataset documentation.

### Additional Info

This dataset contains samples from both training and validation datasets. Modified by TunedIT (converted to ARFF format).

Data type: non-sparse
Number of features: 48
Number of examples and check-sums:
Pos_ex Neg_ex Tot_ex Check_sum
Train  1029  3118  4147 6798109.00
Valid   103   312   415 681151.00

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40993) of an [OpenML dataset](https://www.openml.org/d/40993). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40993/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40993/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40993/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

